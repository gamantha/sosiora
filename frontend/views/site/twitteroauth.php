<?php
namespace app\components;


use yii\vendor\autoload;
use Yii;
use yii\base\Component;
use Abraham\TwitterOAuth\TwitterOAuth;


$oauth_callback = "http://www.sosiora.com/index.php/site/twittercallback";
$url ='';
function getConnectionWithAccessToken($consumer_key, $consumer_secret, $access_token, $access_token_secret) {
  $connection = new TwitterOAuth($consumer_key, $consumer_secret, $access_token, $access_token_secret);
  return $connection;
}
$connection = getConnectionWithAccessToken(Yii::$app->credentials->consumer_key, Yii::$app->credentials->consumer_secret, Yii::$app->credentials->access_token, Yii::$app->credentials->access_token_secret);



echo '<pre>';
print_r($connection);

$request_token = $connection->oauth('oauth/request_token', array('oauth_callback' => $oauth_callback));

//print_r($request_token);
$session = Yii::$app->session;

// check if a session is already open
if ($session->isActive){

} else {
 $session->open();
}

// open a session



$session['oauth_token'] = $request_token['oauth_token'];
$session['oauth_token_secret'] = $request_token['oauth_token_secret'];

//$url = $connection->url('oauth/authorize', array('oauth_token' => $request_token['oauth_token'])); //use the current twiter user
$url = $connection->url('oauth/authenticate', array('oauth_token' => $request_token['oauth_token'])); //always asks for password

return Yii::$app->getResponse()->redirect($url);

?>
<a href="<?php echo $url;?>">continue</a>

