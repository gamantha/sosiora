<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\TwitterUser */

$this->title = Yii::t('app', 'Create Twitter User');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Twitter Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="twitter-user-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
