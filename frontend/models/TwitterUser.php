<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "twitter_user".
 *
 * @property string $id
 * @property string $screen_name
 * @property string $sosiora_id
 * @property string $json
 * @property string $meta
 * @property string $access_token
 * @property integer $access_token_timestamp
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $status
 */
class TwitterUser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'twitter_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sosiora_id', 'access_token_timestamp', 'created_at', 'updated_at'], 'integer'],
            [['json', 'meta', 'access_token', 'status'], 'string'],
            [['screen_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'screen_name' => Yii::t('app', 'Screen Name'),
            'sosiora_id' => Yii::t('app', 'Sosiora ID'),
            'json' => Yii::t('app', 'Json'),
            'meta' => Yii::t('app', 'Meta'),
            'access_token' => Yii::t('app', 'Access Token'),
            'access_token_timestamp' => Yii::t('app', 'Access Token Timestamp'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @inheritdoc
     * @return TwitterUserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TwitterUserQuery(get_called_class());
    }
}
