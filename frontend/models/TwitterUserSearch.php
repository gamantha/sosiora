<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\TwitterUser;

/**
 * TwitterUserSearch represents the model behind the search form about `frontend\models\TwitterUser`.
 */
class TwitterUserSearch extends TwitterUser
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'sosiora_id', 'access_token_timestamp', 'created_at', 'updated_at'], 'integer'],
            [['json', 'meta', 'access_token', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TwitterUser::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'sosiora_id' => $this->sosiora_id,
            'access_token_timestamp' => $this->access_token_timestamp,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'json', $this->json])
            ->andFilterWhere(['like', 'meta', $this->meta])
            ->andFilterWhere(['like', 'access_token', $this->access_token])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
