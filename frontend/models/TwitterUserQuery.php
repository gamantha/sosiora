<?php

namespace frontend\models;

/**
 * This is the ActiveQuery class for [[TwitterUser]].
 *
 * @see TwitterUser
 */
class TwitterUserQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return TwitterUser[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TwitterUser|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
