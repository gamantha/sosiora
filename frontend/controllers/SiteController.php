<?php
namespace frontend\controllers;

use Yii;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use Abraham\TwitterOAuth\TwitterOAuth;
use frontend\models\TwitterUser;


/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        $session = Yii::$app->session;
        $session->remove('access_token');
        unset($session['access_token']);


        $session->close();
        $session->destroy();

           Yii::$app->user->logout();



        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }



    public function actionTwitteroauth()
    {
     return $this->render('twitteroauth');
    }

    public function actionTwittercallback()
    {


      $session = Yii::$app->session;
        if ($session->isActive){
         echo 'session is active';
        } else {
         $session->open();
        }

         if (!isset($session['access_token'])) {
              #		$dataProvider=new CActiveDataProvider('User');
              $request_token = [];
              $request_token['oauth_token'] = $session['oauth_token'];
              $request_token['oauth_token_secret'] = $session['oauth_token_secret'];




              if (isset($_REQUEST['oauth_token']) && $request_token['oauth_token'] !== $_REQUEST['oauth_token']) {
                  echo 'someting is wrong';
              }

              $connection = new TwitterOAuth(Yii::$app->credentials->consumer_key, Yii::$app->credentials->consumer_secret, $request_token['oauth_token'], $request_token['oauth_token_secret']);
              $access_token = $connection->oauth("oauth/access_token", ["oauth_verifier" => $_REQUEST['oauth_verifier']]);
              $session['access_token'] = $access_token;
              $duration=0;               //$duration=$this->rememberMe ? 3600*24*30 :0; // 30 days


         $identity = TwitterUser::findOne(['id' => $session['access_token']['user_id']]);

              if (Yii::$app->user->login($identity))
                  {
                    Yii::$app->getSession()->setFlash('success', 'Login success!!!');
                          return $this->redirect(['site/index']);
                   } else {
                    Yii::$app->getSession()->setFlash('error', 'Login error!!!');
                   }

         } else {
        //  Yii::$app->getSession()->setFlash('warning', 'session still exists');
        echo 'session exists';
        $request_token = $session['access_token'];
        $connection = new TwitterOAuth(Yii::$app->credentials->consumer_key, Yii::$app->credentials->consumer_secret, $request_token['oauth_token'], $request_token['oauth_token_secret']);
         }

         $friend_id = $session['access_token']['user_id'];
           $twitter = TwitterUser::find()->andWhere('id=:id',[':id' => $friend_id])->one();

           if (isset($twitter)) {
            $twitter->access_token = json_encode($session['access_token']);
            $twitter->access_token_timestamp = time();
            $twitter->update();

          } else {
           Yii::$app->getSession()->setFlash('warning', 'That twitter account is not associated with a sosiora user yet. Login with your sosiora account');
           $credentials = $connection->get('account/verify_credentials');
        //echo '<pre>';
        print_r($credentials);
        /*echo '<hr/><hr/><hr/>';
             echo '<hr/><hr/><hr/>';
                  echo '<hr/><hr/><hr/>';
        echo '<pre>';

        print_r($request_token);
        */

        $new_twitter_user = new TwitterUser;
        $new_twitter_user->id = $credentials->id;
                $new_twitter_user->screen_name = $credentials->screen_name;
        $new_twitter_user->json = json_encode($credentials);
        $new_twitter_user->access_token = $request_token['oauth_token'];
        $new_twitter_user->access_token_timestamp = time();
        $new_twitter_user->created_at = time();
        $new_twitter_user->updated_at = time();
        $new_twitter_user->status = 'active';


  $new_twitter_user->sosiora_id = Yii::$app->user->identity->id;

        try {
        $new_twitter_user->save();
                   Yii::$app->getSession()->setFlash('success', 'Your account is successfully associated');
               return $this->redirect(['user/association']);
       }
        catch(\Exception $e){
    throw new \yii\web\HttpException(405, 'Error saving model');
   }



          }
          return $this->render('twittercallback', ['model'=>$twitter]);
     }

     public function actionTwittersearch()
     {
      return $this->render('twittersearch');

     }

}
