<?php

namespace app\components;

use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;

class Sosioradb extends Component
{


public $test = "test";



public $raws_sql = "

select * from raw where
hardware_id IN (\$list_hw_for_sql)
and pin IN (\$pin)
and datetime between '\$begin_string' and '\$end_string'
and verified IN (\$hw_verified)
and status IN (\$hw_status)
and workcode IN (\$hw_workcode)
";

public $absence_sql = "

select *
FROM schedule WHERE ( '\$tanggal_index' BETWEEN '\$begin_string' AND '\$end_string') AND schedule_type = 'weekly'
AND
schedule.day = CAST(dayofweek('\$tanggal_index' ) AS CHAR)
and employee_cat_id IN(\$cat_id)
and optional = 'false'
and if (start_period is not null, date('\$tanggal_index') BETWEEN start_period and finish_period, true)


UNION


select *
FROM schedule WHERE ( '\$tanggal_index' BETWEEN '\$begin_string' AND '\$end_string') AND schedule_type = 'monthly'
AND
schedule.day = CAST(dayofmonth('\$tanggal_index' ) AS CHAR)
and employee_cat_id IN(\$cat_id)
and optional = 'false'
and if (start_period is not null, date('\$tanggal_index') BETWEEN start_period and finish_period, true)


UNION


select *

FROM schedule WHERE ( '\$tanggal_index' BETWEEN '\$begin_string' AND '\$end_string') AND schedule_type = 'yearly'

and employee_cat_id IN(\$cat_id)
and optional = 'false'
and if (start_period is not null, (date_format('\$tanggal_index','%m%d') BETWEEN date_format(start_period,'%m%d') AND date_format(finish_period,'%m%d')), true)

UNION


select *
FROM schedule WHERE ( '\$tanggal_index' BETWEEN '\$begin_string' AND '\$end_string') AND schedule_type = 'onetime'
and employee_cat_id IN(\$cat_id)
and optional = 'false'
and if (start_period is not null, date('\$tanggal_index') BETWEEN start_period and finish_period, true)

";

public $exception_sql = "
select *

from schedule_exception
where
schedule_type = 'weekly' AND


 if (start_period is not null, (('\$begin_string' BETWEEN  start_period AND finish_period) OR
('\$end_string' BETWEEN  start_period AND finish_period)), true)
AND
(day between DAYOFWEEK('\$begin_string') and DAYOFWEEK('\$end_string'))
and employee_cat_id IN(\$cat_id)


UNION

select * from schedule_exception
where
schedule_type = 'monthly' AND

if (start_period is not null, (('\$begin_string' BETWEEN  start_period AND finish_period) OR
('\$end_string' BETWEEN  start_period AND finish_period)), true)
AND
(day between DAYOFMONTH('\$begin_string') and DAYOFMONTH('\$end_string'))
and employee_cat_id IN(\$cat_id)

UNION

select * from schedule_exception
where
schedule_type = 'yearly' AND

 if (start_period is not null,

(
(date_format('\$begin_string','%m%d') BETWEEN date_format(start_period,'%m%d') AND date_format(finish_period,'%m%d'))
OR
(date_format('\$end_string','%m%d') BETWEEN date_format(start_period,'%m%d') AND date_format(finish_period,'%m%d'))
)

, true)
and employee_cat_id IN(\$cat_id)


UNION

select * from schedule_exception
where
schedule_type = 'onetime' AND
 if (start_period is not null, (('\$begin_string' BETWEEN  start_period AND finish_period) OR
('\$end_string' BETWEEN  start_period AND finish_period)), true)
and employee_cat_id IN(\$cat_id)


";
public $scheduled_activity_sql = "

#select * from (select * from (

select * from (select * from (select date(datetime) as tanggal, waktu, hardware_id, pin, schedule_type as type, start_period, finish_period,hari, id,name , employee_cat_id,
IF(time(datetime) between start_time and finish_time,TIMEDIFF(time(datetime),finish_time),TIMEDIFF(time(datetime),finish_time)) as ontime, start_time, finish_time, start_scan, finish_scan,
optional, priority from (
SELECT datetime, hardware_id,pin, dayofweek(datetime) as hari, time(datetime) as waktu
FROM raw WHERE  pin IN (\$pin) AND (datetime BETWEEN '\$begin_string' AND '\$end_string')
AND hardware_id IN (\$list_hw_for_sql)
order by pin, datetime) as A  inner JOIN schedule on schedule_type = 'daily' and employee_cat_id IN(\$cat_id) AND waktu between start_scan and finish_scan
and if (start_period is not null, date(datetime) BETWEEN start_period and finish_period, true)
 order by  tanggal, waktu desc, priority desc
) as B group by tanggal, id order by tanggal asc, waktu asc, start_period desc) as C

UNION

select * from (select * from (select date(datetime) as tanggal, waktu, hardware_id, pin, schedule_type as type, start_period, finish_period,hari, id,name , employee_cat_id,
IF(time(datetime) between start_time and finish_time,TIMEDIFF(time(datetime),finish_time),TIMEDIFF(time(datetime),finish_time)) as ontime, start_time, finish_time, start_scan, finish_scan,
optional, priority from (
SELECT datetime, hardware_id,pin, dayofweek(datetime) as hari, time(datetime) as waktu
FROM raw WHERE  pin IN (\$pin) AND (datetime BETWEEN '\$begin_string' AND '\$end_string')
AND hardware_id IN (\$list_hw_for_sql)
order by pin, datetime) as A  inner JOIN schedule on
schedule.day = CAST(hari AS CHAR) AND schedule_type = 'weekly' and employee_cat_id IN(\$cat_id) AND waktu between start_scan and finish_scan
and if (start_period is not null, date(datetime) BETWEEN start_period and finish_period, true)
 order by  tanggal, waktu desc, priority desc
) as B group by tanggal, id order by tanggal asc, waktu asc, start_period desc) as C

UNION


select * from (select * from (select date(datetime) as tanggal, waktu, hardware_id, pin, schedule_type as type, start_period, finish_period,hari, id,name , employee_cat_id,
IF(time(datetime) between start_time and finish_time,TIMEDIFF(time(datetime),finish_time),TIMEDIFF(time(datetime),finish_time)) as ontime, start_time, finish_time, start_scan, finish_scan, optional, priority from (
SELECT datetime, hardware_id,pin, dayofmonth(datetime) as hari, time(datetime) as waktu
FROM raw WHERE  pin IN (\$pin) AND (datetime BETWEEN '\$begin_string' AND '\$end_string')
AND hardware_id IN (\$list_hw_for_sql)
order by pin, datetime) as A  inner JOIN schedule on
schedule.day = CAST(hari AS CHAR) AND schedule_type = 'monthly' and employee_cat_id IN(\$cat_id) AND waktu between start_scan and finish_scan
and if (start_period is not null, date(datetime) BETWEEN start_period and finish_period, true)
order by  tanggal, waktu desc,  priority desc
) as B group by tanggal, id order by tanggal asc, waktu asc, start_period desc) as C


UNION

select * from (select * from (select date(datetime) as tanggal, waktu, hardware_id, pin, schedule_type as type, start_period, finish_period,hari, id,name , employee_cat_id,
IF(time(datetime) between start_time and finish_time,TIMEDIFF(time(datetime),finish_time),TIMEDIFF(time(datetime),finish_time)) as ontime, start_time, finish_time, start_scan, finish_scan, optional, priority from (
SELECT datetime, hardware_id,pin, dayofmonth(datetime) as hari, time(datetime) as waktu
FROM raw WHERE pin IN (\$pin) AND (date_format(datetime,'%m%d') BETWEEN date_format('\$begin_string','%m%d') AND date_format('\$end_string','%m%d'))

AND hardware_id IN (\$list_hw_for_sql)
order by pin, datetime) as A  inner JOIN schedule on
schedule_type = 'yearly' and employee_cat_id IN(\$cat_id) AND waktu between start_scan and finish_scan
and if (start_period is not null, (date_format(datetime,'%m%d') BETWEEN date_format(start_period,'%m%d') AND date_format(finish_period,'%m%d')), true)

order by  tanggal, waktu desc, priority desc
) as B group by tanggal, id order by tanggal asc, waktu asc, start_period desc) as C


UNION
select * from (select * from (select date(datetime) as tanggal, waktu, hardware_id, pin, schedule_type as type, start_period, finish_period,hari, id,name , employee_cat_id,
IF(time(datetime) between start_time and finish_time,TIMEDIFF(time(datetime),finish_time),TIMEDIFF(time(datetime),finish_time)) as ontime, start_time, finish_time, start_scan, finish_scan, optional, priority from (
SELECT datetime, hardware_id,pin, dayofmonth(datetime) as hari, time(datetime) as waktu
FROM raw WHERE  pin IN (\$pin) AND (datetime BETWEEN '\$begin_string' AND '\$end_string')
AND hardware_id IN (\$list_hw_for_sql)
 order by pin, datetime) as A  inner JOIN schedule on
schedule_type = 'onetime' and employee_cat_id IN (\$cat_id) AND waktu between start_scan and finish_scan
and if (start_period is not null, date(datetime) BETWEEN start_period and finish_period, true)
order by  tanggal, waktu desc,
priority desc
) as B group by tanggal, id order by tanggal asc, waktu asc, start_period desc) as C


";

/*
public $activity_sql = "
select * from (select * from (
 select * from (select * from (select date(datetime) as tanggal, waktu, hardware_id, pin, schedule_type as type, start_period, finish_period,hari, id,name , employee_cat_id,
IF(time(datetime) between start_time and finish_time,TIMEDIFF(time(datetime),finish_time),TIMEDIFF(time(datetime),finish_time)) as ontime, start_time, finish_time, start_scan, finish_scan, optional, priority, schedule.order from (
SELECT datetime, hardware_id,pin, dayofweek(datetime) as hari, time(datetime) as waktu
FROM raw WHERE  pin IN (\$pin) AND (datetime BETWEEN '\$begin_string' AND '\$end_string')
AND hardware_id IN (\$list_hw_for_sql)
order by pin, datetime) as A  inner JOIN schedule on
schedule.day = CAST(hari AS CHAR) AND schedule_type = 'weekly' and employee_cat_id IN(\$cat_id) AND waktu between start_scan and finish_scan
and if (start_period is not null, date(datetime) BETWEEN start_period and finish_period, true)
 order by  tanggal, waktu desc, FIELD(optional, 'true','false'), priority desc
) as B group by tanggal, id order by tanggal asc, waktu asc, start_period desc, FIELD(optional, 'true','false')) as C


UNION

select * from (select * from (select date(datetime) as tanggal, waktu, hardware_id, pin, schedule_type as type, start_period, finish_period,hari, id,name , employee_cat_id,
IF(time(datetime) between start_time and finish_time,TIMEDIFF(time(datetime),finish_time),TIMEDIFF(time(datetime),finish_time)) as ontime, start_time, finish_time, start_scan, finish_scan, optional, priority, schedule.order from (
SELECT datetime, hardware_id,pin, dayofmonth(datetime) as hari, time(datetime) as waktu
FROM raw WHERE pin IN (\$pin) AND (datetime BETWEEN '\$begin_string' AND '\$end_string')
AND hardware_id IN ('\$list_hw_for_sql')
order by pin, datetime) as A  inner JOIN schedule on
schedule.day = CAST(hari AS CHAR) AND schedule_type = 'monthly' and employee_cat_id IN(\$cat_id) AND waktu between start_scan and finish_scan
and if (start_period is not null, date(datetime) BETWEEN start_period and finish_period, true)
order by  tanggal, waktu desc, FIELD(optional, 'true','false'), priority desc
) as B group by tanggal, id order by tanggal asc, waktu asc, start_period desc, FIELD(optional, 'true','false')) as C



UNION

select * from (select * from (select date(datetime) as tanggal, waktu, hardware_id, pin, schedule_type as type, start_period, finish_period,hari, id,name , employee_cat_id,
IF(time(datetime) between start_time and finish_time,TIMEDIFF(time(datetime),finish_time),TIMEDIFF(time(datetime),finish_time)) as ontime, start_time, finish_time, start_scan, finish_scan, optional, priority, schedule.order from (
SELECT datetime, hardware_id,pin, dayofmonth(datetime) as hari, time(datetime) as waktu
FROM raw WHERE pin IN (\$pin) AND (date_format(datetime,'%m%d') BETWEEN date_format('\$begin_string','%m%d') AND date_format('\$end_string','%m%d'))

AND hardware_id IN ('\$list_hw_for_sql')
order by pin, datetime) as A  inner JOIN schedule on
schedule_type = 'yearly' and employee_cat_id IN(\$cat_id) AND waktu between start_scan and finish_scan
and if (start_period is not null, date(datetime) BETWEEN start_period and finish_period, true)
AND (date_format(datetime,'%m%d') BETWEEN date_format(start_period,'%m%d') AND date_format(finish_period,'%m%d'))

order by  tanggal, waktu desc, FIELD(optional, 'true','false'), priority desc
) as B group by tanggal, id order by tanggal asc, waktu asc, start_period desc, FIELD(optional, 'true','false')) as C



UNION

select * from (select * from (select date(datetime) as tanggal, waktu, hardware_id, pin, schedule_type as type, start_period, finish_period,hari, id,name , employee_cat_id,
IF(time(datetime) between start_time and finish_time,TIMEDIFF(time(datetime),finish_time),TIMEDIFF(time(datetime),finish_time)) as ontime, start_time, finish_time, start_scan, finish_scan, optional, priority, schedule.order from (
SELECT datetime, hardware_id,pin, dayofmonth(datetime) as hari, time(datetime) as waktu
FROM raw WHERE pin IN (\$pin) AND
(datetime BETWEEN '\$begin_string' AND '\$end_string') AND hardware_id IN ('\$list_hw_for_sql')
 order by pin, datetime) as A  inner JOIN schedule on
schedule_type = 'onetime' and employee_cat_id IN(\$cat_id) AND waktu between start_scan and finish_scan
and if (start_period is not null, date(datetime) BETWEEN start_period and finish_period, true)
 AND (date_format(datetime,'%m%d') BETWEEN date_format(start_period,'%m%d') AND date_format(finish_period,'%m%d'))
order by  tanggal, waktu desc, FIELD(optional, 'true','false'), priority desc
) as B group by tanggal, id order by tanggal asc, waktu asc, start_period desc, FIELD(optional, 'true','false')) as C


) as D  order by tanggal asc, waktu asc, FIELD(type, 'onetime','yearly','monthly','weekly'),
start_period desc, FIELD(optional, 'true','false'),  priority desc, D.order desc ) as E where priority is not null and E.order is not null group by tanggal, waktu
";
*/


public $daily_exception_sql ="



select * from schedule_exception
where
schedule_type = 'weekly' AND


 if (start_period is not null, (('\$tanggal_index' BETWEEN  start_period AND finish_period)), true)
AND
(day = DAYOFWEEK(date('\$tanggal_index')))
and employee_cat_id IN(\$cat_id)


UNION


select * from schedule_exception
where
schedule_type = 'monthly' AND


 if (start_period is not null, (('\$tanggal_index' BETWEEN  start_period AND finish_period)), true)
AND
(day = DAYOFMONTH(date('\$tanggal_index')))
and employee_cat_id IN(\$cat_id)

UNION


select * from schedule_exception
where
schedule_type = 'yearly'

AND


 if (start_period is not null, ((
date_format('\$tanggal_index','%m%d') BETWEEN  date_format(start_period,'%m%d') AND date_format(finish_period,'%m%d'))), true)
and employee_cat_id IN(\$cat_id)

UNION


select * from schedule_exception
where
schedule_type = 'onetime' AND

 if (start_period is not null, (('\$tanggal_index' BETWEEN  start_period AND finish_period)), true)
and employee_cat_id IN(\$cat_id)



";

public $attendance_sql = "
select F.tanggal, F.waktu as masuk, z.waktu as pulang, f.hari,
F.id as schedule_id1, Z.id as schedule_id2, F.priority as priority1,Z.priority as priority2,F.ontime as ontime1, Z.ontime as ontime2 ,
F.start_time as start_time1,
Z.start_time as start_time2,
F.finish_time as finish_time1,
Z.finish_time as finish_time2

 from (

select * from (select * from (
 select * from (select * from (select date(datetime) as tanggal, waktu, hardware_id, pin, schedule_type as type, start_period, finish_period,hari, id,name ,
employee_cat_id, IF(time(datetime) between start_time and finish_time,TIMEDIFF(time(datetime),finish_time), TIMEDIFF(time(datetime),finish_time)) as ontime, start_time, finish_time, start_scan, finish_scan, optional, priority from (
SELECT datetime, hardware_id,pin, dayofweek(datetime) as hari, time(datetime) as waktu
FROM raw WHERE           			pin IN (\$pin) AND (               			datetime BETWEEN '\$begin_string'
               			AND '\$end_string')
AND hardware_id IN (\$list_hw_for_sql)
order by pin, datetime) as A  inner JOIN schedule on
schedule.day = CAST(hari AS CHAR) AND schedule_type = 'weekly' and employee_cat_id IN(\$cat_id) AND waktu between start_scan and finish_scan
and if (start_period is not null, date(datetime) BETWEEN start_period and finish_period, true)
 order by  tanggal, waktu desc, FIELD(optional, 'true','false'), priority desc
) as B group by tanggal, id order by tanggal asc, waktu asc, start_period desc, FIELD(optional, 'true','false')) as C


UNION

select * from (select * from (select date(datetime) as tanggal, waktu, hardware_id, pin, schedule_type as type, start_period, finish_period,hari, id,name , employee_cat_id,
IF(time(datetime) between start_time and finish_time,TIMEDIFF(time(datetime),finish_time),TIMEDIFF(time(datetime),finish_time)) as ontime, start_time, finish_time, start_scan, finish_scan, optional, priority from (
SELECT datetime, hardware_id,pin, dayofmonth(datetime) as hari, time(datetime) as waktu
FROM raw WHERE pin IN (\$pin) AND (datetime BETWEEN '\$begin_string' AND '\$end_string')
AND hardware_id IN (\$list_hw_for_sql)
order by pin, datetime) as A  inner JOIN schedule on
schedule.day = CAST(hari AS CHAR) AND schedule_type = 'monthly' and employee_cat_id IN(\$cat_id) AND waktu between start_scan and finish_scan
and if (start_period is not null, date(datetime) BETWEEN start_period and finish_period, true)
order by  tanggal, waktu desc, FIELD(optional, 'true','false'), priority desc
) as B group by tanggal, id order by tanggal asc, waktu asc, start_period desc, FIELD(optional, 'true','false')) as C



UNION

select * from (select * from (select date(datetime) as tanggal, waktu, hardware_id, pin, schedule_type as type, start_period, finish_period,hari, id,name , employee_cat_id,
IF(time(datetime) between start_time and finish_time,TIMEDIFF(time(datetime),finish_time),TIMEDIFF(time(datetime),finish_time)) as ontime, start_time, finish_time, start_scan, finish_scan, optional, priority from (
SELECT datetime, hardware_id,pin, dayofmonth(datetime) as hari, time(datetime) as waktu
FROM raw WHERE pin IN (\$pin) AND (date_format(datetime,'%m%d') BETWEEN date_format('\$begin_string','%m%d') AND date_format('\$end_string','%m%d'))

AND hardware_id IN (\$list_hw_for_sql)
order by pin, datetime) as A  inner JOIN schedule on
schedule_type = 'yearly' and employee_cat_id IN(\$cat_id) AND waktu between start_scan and finish_scan
and if (start_period is not null, date(datetime) BETWEEN start_period and finish_period, true)
AND (date_format(datetime,'%m%d') BETWEEN date_format(start_period,'%m%d') AND date_format(finish_period,'%m%d'))

order by  tanggal, waktu desc, FIELD(optional, 'true','false'), priority desc
) as B group by tanggal, id order by tanggal asc, waktu asc, start_period desc, FIELD(optional, 'true','false')) as C



UNION

select * from (select * from (select date(datetime) as tanggal, waktu, hardware_id, pin, schedule_type as type, start_period, finish_period,hari, id,name , employee_cat_id,
IF(time(datetime) between start_time and finish_time,TIMEDIFF(time(datetime),finish_time),TIMEDIFF(time(datetime),finish_time)) as ontime, start_time, finish_time, start_scan, finish_scan, optional, priority from (
SELECT datetime, hardware_id,pin, dayofmonth(datetime) as hari, time(datetime) as waktu
FROM raw WHERE pin IN (\$pin) AND
(datetime BETWEEN '\$begin_string' AND '\$end_string') AND hardware_id IN (\$list_hw_for_sql)
 order by pin, datetime) as A  inner JOIN schedule on
schedule_type = 'onetime' and employee_cat_id IN(\$cat_id) AND waktu between start_scan and finish_scan
and if (start_period is not null, date(datetime) BETWEEN start_period and finish_period, true)
 AND (date_format(datetime,'%m%d') BETWEEN date_format(start_period,'%m%d') AND date_format(finish_period,'%m%d'))
order by  tanggal, waktu desc, FIELD(optional, 'true','false'), priority desc
) as B group by tanggal, id order by tanggal asc, waktu asc, start_period desc, FIELD(optional, 'true','false')) as C


) as D  order by tanggal asc, waktu asc, FIELD(type, 'onetime','yearly','monthly','weekly'),
start_period desc, FIELD(optional, 'true','false'),  priority desc ) as E where priority is not null group by tanggal, waktu

) as F inner JOIN
(select * from (select * from (
 select * from (select * from (select date(datetime) as tanggal, waktu, hardware_id, pin, schedule_type as type, start_period, finish_period,hari, id,name , employee_cat_id,
IF(time(datetime) between start_time and finish_time,
TIMEDIFF(time(datetime),start_time),
TIMEDIFF(time(datetime),start_time)) as ontime,
start_time, finish_time, start_scan, finish_scan, optional, priority from (
SELECT datetime, hardware_id,pin, dayofweek(datetime) as hari, time(datetime) as waktu
FROM raw WHERE           			pin IN (\$pin) AND (               			datetime BETWEEN '\$begin_string'
               			AND '\$end_string')
AND hardware_id IN (\$list_hw_for_sql)
order by pin, datetime) as A  inner JOIN schedule on
schedule.day = CAST(hari AS CHAR) AND schedule_type = 'weekly' and employee_cat_id IN(\$cat_id) AND waktu between start_scan and finish_scan
and if (start_period is not null, date(datetime) BETWEEN start_period and finish_period, true)
 order by  tanggal, waktu desc, FIELD(optional, 'true','false'), priority desc
) as B group by tanggal, id order by tanggal asc, waktu asc, start_period desc, FIELD(optional, 'true','false')) as C


UNION

select * from (select * from (select date(datetime) as tanggal, waktu, hardware_id, pin, schedule_type as type, start_period, finish_period,hari, id,name , employee_cat_id,
IF(time(datetime) between start_time and finish_time,
TIMEDIFF(time(datetime),start_time),
TIMEDIFF(time(datetime),start_time)) as ontime,

start_time, finish_time, start_scan, finish_scan, optional, priority from (
SELECT datetime, hardware_id,pin, dayofmonth(datetime) as hari, time(datetime) as waktu
FROM raw WHERE pin IN (\$pin) AND (datetime BETWEEN '\$begin_string' AND '\$end_string')
AND hardware_id IN (\$list_hw_for_sql)
order by pin, datetime) as A  inner JOIN schedule on
schedule.day = CAST(hari AS CHAR) AND schedule_type = 'monthly' and employee_cat_id IN(\$cat_id) AND waktu between start_scan and finish_scan
and if (start_period is not null, date(datetime) BETWEEN start_period and finish_period, true)
order by  tanggal, waktu desc, FIELD(optional, 'true','false'), priority desc
) as B group by tanggal, id order by tanggal asc, waktu asc, start_period desc, FIELD(optional, 'true','false')) as C



UNION

select * from (select * from (select date(datetime) as tanggal, waktu, hardware_id, pin, schedule_type as type, start_period, finish_period,hari, id,name , employee_cat_id,
IF(time(datetime) between start_time and finish_time,
TIMEDIFF(time(datetime),start_time),
TIMEDIFF(time(datetime),start_time)) as ontime,
start_time, finish_time, start_scan, finish_scan, optional, priority from (
SELECT datetime, hardware_id,pin, dayofmonth(datetime) as hari, time(datetime) as waktu
FROM raw WHERE pin IN (\$pin) AND (date_format(datetime,'%m%d') BETWEEN date_format('\$begin_string','%m%d') AND date_format('\$end_string','%m%d'))

AND hardware_id IN (\$list_hw_for_sql)
order by pin, datetime) as A  inner JOIN schedule on
schedule_type = 'yearly' and employee_cat_id IN(\$cat_id) AND waktu between start_scan and finish_scan
and if (start_period is not null, date(datetime) BETWEEN start_period and finish_period, true)
AND (date_format(datetime,'%m%d') BETWEEN date_format(start_period,'%m%d') AND date_format(finish_period,'%m%d'))

order by  tanggal, waktu desc, FIELD(optional, 'true','false'), priority desc
) as B group by tanggal, id order by tanggal asc, waktu asc, start_period desc, FIELD(optional, 'true','false')) as C



UNION

select * from (select * from (select date(datetime) as tanggal, waktu, hardware_id, pin, schedule_type as type, start_period, finish_period,hari, id,name , employee_cat_id,
IF(time(datetime) between start_time and finish_time,
TIMEDIFF(time(datetime),start_time),
TIMEDIFF(time(datetime),start_time)) as ontime,
start_time, finish_time, start_scan, finish_scan, optional, priority from (
SELECT datetime, hardware_id,pin, dayofmonth(datetime) as hari, time(datetime) as waktu
FROM raw WHERE pin IN (\$pin) AND
(datetime BETWEEN '\$begin_string' AND '\$end_string') AND hardware_id IN (\$list_hw_for_sql)
 order by pin, datetime) as A  inner JOIN schedule on
schedule_type = 'onetime' and employee_cat_id IN(\$cat_id) AND waktu between start_scan and finish_scan
and if (start_period is not null, date(datetime) BETWEEN start_period and finish_period, true)
 AND (date_format(datetime,'%m%d') BETWEEN date_format(start_period,'%m%d') AND date_format(finish_period,'%m%d'))
order by  tanggal, waktu desc, FIELD(optional, 'true','false'), priority desc
) as B group by tanggal, id order by tanggal asc, waktu asc, start_period desc, FIELD(optional, 'true','false')) as C


) as D  order by tanggal asc, waktu asc, FIELD(type, 'onetime','yearly','monthly','weekly'),
start_period desc, FIELD(optional, 'true','false'),  priority desc ) as E where priority is not null group by tanggal, waktu) as Z
on (F.id <> Z.id) where (F.waktu <= Z.waktu) and  F.tanggal = Z.tanggal

AND F.priority = Z.priority AND F.hari = Z.hari
order by F.tanggal, Z.tanggal

";

public $permission_approved_sql = "

select
request.id as request_id,
permission.id as permission_id,
timestamp,
request.status,
hardware_id,
hardware_verified,
hardware_status,
hardware_workcode,
employee_id,
permission_type_id,
permission.from,
permission.to,
reason


 from request inner join permission
on permission_id = permission.id where
request.status = 'approved'
and hardware_id IN (\$list_hw_for_sql)
" .
//and hardware_verified IN (\$hw_verified)
//and hardware_status IN (\$hw_status)
//and hardware_workcode IN (\$hw_workcode)

"
and employee_id IN (\$employee_id)
and (permission.from between '\$begin_string' and '\$end_string'
OR permission.to between '\$begin_string' and '\$end_string')
";


public $schedule_exist_check_by_date = "

select id,name,employee_cat_id,start_period,finish_period,schedule_type, day,start_time,finish_time,start_scan,finish_scan,optional,priority  from (

select id,name,employee_cat_id,start_period,finish_period,schedule_type, day,start_time,finish_time,start_scan,finish_scan,optional,priority from schedule where employee_cat_id IN(\$cat_id) AND
 if (start_period is not null, date('\$date') BETWEEN start_period and finish_period, true)
and schedule_type = 'daily' and optional = 'false'


UNION


select id,name,employee_cat_id,start_period,finish_period,schedule_type, day,start_time,finish_time,start_scan,finish_scan,optional,priority from schedule where employee_cat_id IN(\$cat_id) AND
 if (start_period is not null, date('\$date') BETWEEN start_period and finish_period, true)
and schedule_type = 'weekly' and day = CAST(DAYOFWEEK('\$date') AS CHAR) and optional = 'false'


UNION

select id,name,employee_cat_id,start_period,finish_period,schedule_type, day,start_time,finish_time,start_scan,finish_scan,optional,priority from schedule where employee_cat_id IN(\$cat_id) AND
 if (start_period is not null, date('\$date') BETWEEN start_period and finish_period, true)
and schedule_type = 'monthly' and day = CAST(DAYOFMONTH('\$date') AS CHAR) and optional = 'false'

UNION

select id,name,employee_cat_id,start_period,finish_period,schedule_type, day,start_time,finish_time,start_scan,finish_scan,optional,priority  from schedule where employee_cat_id IN(\$cat_id) AND
 if (start_period is not null, (date_format('\$date', '%m%d') BETWEEN date_format(start_period, '%m%d') and date_format(finish_period, '%m%d')), true)
and schedule_type = 'yearly' and optional = 'false'


UNION

select id,name,employee_cat_id,start_period,finish_period,schedule_type, day,start_time,finish_time,start_scan,finish_scan,optional,priority  from schedule where employee_cat_id IN(\$cat_id) AND
 if (start_period is not null, (date('\$date') BETWEEN start_period and finish_period), true)
and schedule_type = 'onetime' and optional = 'false'
) as a



";


public $permission_sql = "
select
tanggal,
A.id,
A.employee_id,
A.permission_type_id,
A.from,
A.to,
A.reason,
A.name,
A.request_timeout,
A.company_id,
request.timestamp,
request.status,
request.hardware_id,
request.hardware_workcode,
request.hardware_verified,
request.hardware_status

from (

select perm.tanggal,
perm.id,
perm.employee_id,
perm.permission_type_id,
perm.from,
perm.to,
perm.reason,perm.name, perm.request_timeout,perm.company_id
from (
  select ('\$tanggal_index') as tanggal,permission.id, permission.employee_id, permission.permission_type_id, permission.from, permission.to, permission.reason, permission_type.name, permission_type.request_timeout, permission_type.company_id from (
  permission INNER JOIN permission_type on permission.permission_type_id = permission_type.id)
) as perm
  WHERE
    (
       perm.employee_id IN ('\$employee_id')
       AND
       ('\$tanggal_index' BETWEEN perm.from AND perm.to)
    )
) as A
inner join request where
A.id = request.permission_id
AND request.status IN ('\$status')
";

}


?>
