<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
     'credentials' => [
        'class' => 'app\components\Credentials',

     ],


     'sosioradb' => [
       'class' => 'app\components\Sosioradb',
     ],
     'mailer' => [
    'class' => 'yii\swiftmailer\Mailer',
         'useFileTransport' => false,
         'transport' => [
    'class' => 'Swift_SmtpTransport',
    'host' => 'smtp.gmail.com',  // e.g. smtp.mandrillapp.com or smtp.gmail.com
    'username' => 'gamantha.adhiguna@gmail.com',
    'password' => 'gamantha123!',
    'port' => '587', // Port 25 is a very common port too
    'encryption' => 'tls', // It is often used, check your provider or mail server specs
    ],
],


        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
        'urlManager' => [
          'enablePrettyUrl' => true,
          'showScriptName' => true,
          'enableStrictParsing' => true,
          'rules' => [
           '<controller:\w+>/<id:\d+>'=>'<controller>/view',
           '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
           '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
            'http://dev.sosiora.com' => 'site/index',

          ],
    ],
    ],
    'modules' => [
     'gii' => [
         'class' => 'yii\gii\Module',
         'allowedIPs' => ['127.0.0.1', '::1', '*'] // adjust this to your needs,
         //'password' => '123456'
     ],


    ],
    'params' => $params,
];
